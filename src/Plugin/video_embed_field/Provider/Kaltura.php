<?php

namespace Drupal\video_embed_kaltura\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * Kaltura supporting code.
 *
 * @VideoEmbedProvider(
 *   id = "kaltura",
 *   title = @Translation("Kaltura")
 * )
 */
class Kaltura extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {

    $config = \Drupal::configFactory()->getEditable('video_embed_kaltura.kaltura_config');
    $partner_id = $config->get('partner_id');
    $uiconf_id = $config->get('uiconf_id');

    $autoplay_flag = 'false';
    if ($autoplay) {
      $autoplay_flag = 'true';
    }
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'src' => sprintf('https://cdnapisec.kaltura.com/p/%d/sp/%s/embedIframeJs/uiconf_id/%d/partner_id/%d?iframeembed=true&playerId=kaltura_player&entry_id=%s&flashvars[streamerType]=auto&amp;flashvars[localizationCode]=en&amp;flashvars[leadWithHTML5]=true&amp;flashvars[autoPlay]=%s&amp;flashvars[sideBarContainer.plugin]=true&amp;flashvars[sideBarContainer.position]=left&amp;flashvars[sideBarContainer.clickToClose]=true&amp;flashvars[chapters.plugin]=true&amp;flashvars[chapters.layout]=vertical&amp;flashvars[chapters.thumbnailRotator]=false&amp;flashvars[streamSelector.plugin]=true&amp;flashvars[EmbedPlayer.SpinnerTarget]=videoHolder&amp;flashvars[dualScreen.plugin]=true&amp;&wid=0_8noammrl', $partner_id, $partner_id . '00', $uiconf_id, $partner_id, $this->getVideoId(), $autoplay_flag),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $thumbnail = sprintf('https://cfvod.kaltura.com/p/811441/sp/81144100/thumbnail/entry_id/%s/version/100011/width/300/height/150', $this->getVideoId());
    return $thumbnail;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {

    // Parse_url is an easy way to break a url into its components.
    $parsed = parse_url($input);

    // 12 is the number of charecters in  kaltura.com.
    $check_kaltura_domain = substr($parsed['host'], (strlen($parsed['host']) - 11));
    if ($check_kaltura_domain == 'kaltura.com') {
      // Return entry id if video embedded by iframe's src attribute.
      if (!empty($parsed['query'])) {
        $query = [];
        parse_str($parsed['query'], $query);
        if (isset($query['entry_id'])) {
          return $query['entry_id'];
        }
      }

      $path = $parsed['path'];
      $parts = explode('/', $path);
      foreach ($parts as $key => $part) {
        if ($part == 'id') {
          return $parts[$key + 1];
        }

        if ($part == 'media') {
          return $parts[$key + 2];
        }

        if ($part == 'entry_id') {
          return $parts[$key + 1];
        }
      }
      return FALSE;
    }
  }

}
